package dragonVSvillage;

public class RandomGenerator 
{

	private static RandomGenerator instance ;
	
	private MRG32k3a randomer ; 
	private static int seed = 12345 ;
	
	public static void setSeed(int pSeed)
	{  
		seed = pSeed ;
		instance = new RandomGenerator() ;
	}
	
	
	
	private RandomGenerator()
	{ this.randomer = new MRG32k3a(seed) ; }
	
	
	
	private double getRandom()
	{ return this.randomer.nextDouble() ; }
	
	
	
	
	
	
	public static double getRand()
	{
		if (instance == null)
			instance = new RandomGenerator() ;
		
		return instance.getRandom() ;
	}
	
	
	
	public static int getRand(int a , int b)
	{
		if (instance == null)
			instance = new RandomGenerator() ;
		
		return (int)Math.floor( a+(instance.getRandom()*(b-a+1)) ) ;
	}
	
	
	
	public static double getRand(double a , double b)
	{
		if (instance == null)
			instance = new RandomGenerator() ;
		
		return  (a+(instance.getRandom()*(b-a) )) ;
	}
}
