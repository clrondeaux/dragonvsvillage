package dragonVSvillage;

import java.awt.Label;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JLabel;

import com.opencsv.CSVWriter;

import Agents.Agent;
import Agents.Dragon;
import Agents.Village;
import Agents.Ville;
import InterfaceGraphique.MyWindow;

public class Carte extends MyWindow
{
	public static boolean estEnModeNoStop = true ;
	
	public static boolean routineEnCours = true ;
	
	
	
	/**
	 * Liste des agents sur la carte
	 */
	protected static ArrayList<Agent> agents ;
	
	/**
	 * Liste tempon qui permet de r� ordonner les agents entre chaque �tapes de la routine
	 * ( pour ne pas donner d'avantage � un agent car il serait toujours en d�but de liste)
	 */
	private static ArrayList<Agent> newOrder ;
	
	
	
	/**
	 * Liste tempon des agents � ajouter sur la liste agents � la fin de la rotuine 
	 */
	private static ArrayList<Agent> aAjouter ;
	
	/**
	 * Liste tempon des agents � retirer sur la liste agents � la fin de la rotuine 
	 */
	private static ArrayList<Agent> aRetirer ;
	
	/**
	 * Permet de signifier qu'a la fin de cette routine, il faudra ajouter l'agent a � la liste d'agents de la carte
	 * 
	 * @param a Agent � ajouter
	 */
	public static void naissanceAgent(Agent a)
	{ Carte.aAjouter.add(a) ; }
	
	/**
	 * Permet de signifier qu'a la fin de cette routine, il faudra retirer l'agent a de la liste d'agents de la carte
	 * 
	 * @param a Agent � retirer
	 */
	public static void mortAgent(Agent a)
	{ Carte.aRetirer.add(a) ; }
	
	/**
	 * Parcours toute la liste aAjouter pour ajouter tout les agents de cette liste � agents
	 */
	private static void ajouterToutAgentNee()
	{
		Agent a ;
		if ( ! Carte.aAjouter.isEmpty() )
		{
			Iterator<Agent> iter = Carte.aAjouter.iterator();
			while (iter.hasNext())
			{
				a = iter.next() ;
				Carte.ajouterAgent( a ) ;
				Carte.agents.add( a ) ;
					
				// Si l'agent � ajouter est un village, on test ses alentours pour voir si on doit tout transformer en ville ou pas
				if ( a instanceof Village )
				{	
					if      ( ajouterVille( +1 , -1 , a ) ) {} // On test en haut � droite
					else if ( ajouterVille( -1 , -1 , a ) ) {} // On test en haut � gauche
					else if ( ajouterVille( -1 , +1 , a ) ) {} // On test en bas � gauche
					else if ( ajouterVille( +1 , +1 , a ) ) {} // On test en bas � droite
				} 
			}
		}
		Carte.aAjouter.clear();
	}

	private static boolean ajouterVille(int decalX , int decalY , Agent a)
	{
		boolean onAjouteUneVille = false ;
		
		// On test le si le carr� autours sont tous des village
		if ( Carte.getAgentPos(a.getX()+decalX , a.getY()) instanceof Village && Carte.getAgentPos(a.getX()+decalX , a.getY()+decalY) instanceof Village && Carte.getAgentPos(a.getX() , a.getY()+decalY) instanceof Village )
		{
			// On v�rifie que ce sont bien des villages et pas des villes (CF api Java instanceof reloux)
			if ( !(Carte.getAgentPos(a.getX()+decalX , a.getY()) instanceof Ville) && !(Carte.getAgentPos(a.getX()+decalX , a.getY()+decalY) instanceof Ville) && !(Carte.getAgentPos(a.getX() , a.getY()+decalY) instanceof Ville) )
			{
				Ville v = new Ville() ;
				v.setPos(a.getX(),a.getY()  ,  a.getX()+decalX,a.getY()  , a.getX()+decalX,a.getY()+decalY  ,  a.getX(),a.getY()+decalY);
				
				Carte.agents.remove( Carte.getAgentPos(a.getX()+decalX , a.getY()) ) ;
				Carte.enleverAgent(  Carte.getAgentPos(a.getX()+decalX , a.getY()) ) ;
				Carte.agents.remove( Carte.getAgentPos(a.getX()+decalX , a.getY()+decalY) ) ;
				Carte.enleverAgent(  Carte.getAgentPos(a.getX()+decalX , a.getY()+decalY) ) ;
				Carte.agents.remove( Carte.getAgentPos(a.getX()        , a.getY()+decalY) ) ;
				Carte.enleverAgent(  Carte.getAgentPos(a.getX()        , a.getY()+decalY) ) ;
				Carte.agents.remove( a ) ; 
				Carte.enleverAgent(  a ) ;
				
				Carte.ajouterAgent(v.getX() , v.getY() , v) ;
				Carte.ajouterAgent(v.getX2() , v.getY2() , v) ;
				Carte.ajouterAgent(v.getX3() , v.getY3() , v) ;
				Carte.ajouterAgent(v.getX4() , v.getY4() , v) ;
				Carte.agents.add( v ) ;
		
				onAjouteUneVille = true ;
			}
		}
		
		return onAjouteUneVille;
	}

	
	/**
	 * Parcours toute la liste aRetirer pour retirer tout les agents de cette liste de la liste agents
	 */
	private static void retirerToutAgentsMort()
	{
		Agent a ;
		if ( ! Carte.aRetirer.isEmpty() )
		{
			Iterator<Agent> iter = Carte.aRetirer.iterator();
			while (iter.hasNext())
			{
				a = iter.next() ;
				a.saveVarRecolte() ;
				Carte.enleverAgent( a ) ;
				Carte.agents.remove( a ) ;
			}
		}
		Carte.aRetirer.clear();
	}
	
	

	
	// ============================================================================================================================ //
	
	
	
	
	/**
	 * Matrice regroupant les positions de chaque agent (permet de savoir les voisins de chaque agent)
	 */
	private static Agent[][] carte ; 
	
	/**
	 * GETTER
	 * Fonction permettant d'acc�der � l'agent � la position demander (MAY RETURN NULL)
	 * 
	 * @param px 	La position X de l� ou on cherche un agent
	 * @param py 	La position Y de l� ou on cherche un agent
	 * 
	 * @return	Le potentiel agent � cette position (null si rien ou en dehors carte)
	 */
	public static Agent getAgentPos( int px , int py )
	{ 
		if ((0 <= px && px < Carte.getLarg()) && (0 <= py && py < Carte.getHaut()))
			return Carte.getCarte()[px][py] ;
		else
			return null ;
	}
	
	/**
	 * SETTER
	 * Permet de retirer un agent qui �tait anciennement � la position [px , py]
	 * 
	 * @param px	La position X de l� ou on veut retirer l'Agent
	 * @param py 	La position Y de l� ou on veut retirer l'Agent
	 */
	public static void enleverAgent(int px , int py)
	{
		if ((0 <= px && px < Carte.getLarg()) && (0 <= py && py < Carte.getHaut()))
			Carte.getCarte()[px][py] = null ;
	}
	
	/**
	 * SETTER
	 * Permet de retirer un agent qui �tait anciennement � la position [px , py]
	 * 
	 * @param a L'agent qu'on veut retirer
	 */
	public static void enleverAgent(Agent a)
	{ 
		if (a instanceof Ville)	
		{
			// Si c'est une ville, on doit retirer 4 cases de la carte, et on ne sait pas d'avance dans quel sens aller
			Carte.enleverAgent(a.getX(), a.getY()) ;
			
			Carte.enleverAgent( ((Ville)a).getX2() , ((Ville)a).getY2() ) ;
			Carte.enleverAgent( ((Ville)a).getX3() , ((Ville)a).getY3() ) ;
			Carte.enleverAgent( ((Ville)a).getX4() , ((Ville)a).getY4() ) ;
		}
		else
			Carte.enleverAgent(a.getX(), a.getY()) ;
	}
	
	
	
	/**
	 * SETTER
	 * Permet d'ajouter la nouvelle position d'un agent � la position [px , py]
	 * 
	 * @param px	La position X de l� ou on veut ajouter l'Agent
	 * @param py 	La position Y de l� ou on veut ajouter l'Agent
	 * @param a		L'agent qu'on veut ajouter
	 */
	public static void ajouterAgent(int px , int py , Agent a)
	{
		if ((0 <= px && px < Carte.getLarg()) && (0 <= py && py < Carte.getHaut()))
			Carte.getCarte()[px][py] = a ;
	}
	
	/**
	 * SETTER
	 * Permet d'ajouter la nouvelle position d'un agent � sa position actuelle (a.getX() et a.getY())
	 * 
	 * @param a	L'agent qu'on veut ajouter
	 */
	public static void ajouterAgent(Agent a)
	{ Carte.ajouterAgent(a.getX(), a.getY() , a); } 
	
	
	
	/**
	 * SETTER
	 * Permet de retirer l'agent de la position [px , py] et de l'ajouter � son actuelle
	 * 
	 * @param px	Ancienne position X de l'agent
	 * @param py	Ancienne position Y de l'agent
	 * @param a		Agent � deplacer
	 */
	public static void deplacerAgent(int px , int py , Agent a)
	{
		Carte.enleverAgent(px, py);
		Carte.ajouterAgent(a.getX(), a.getY(), a) ;
	}
	
	
	
	/**
	 * GETTER
	 * Permet de savoir si on peut se deplacer � cet endroit
	 * Le seul crit�re permettant de savoir si on peut se d�placer et de savoir s'il y a d�j� quelque chose dans cette case
	 * 
	 * @param px La postion X de l� o� on veut tester
	 * @param py La postion Y de l� o� on veut tester
	 * 
	 * @return	TRUE = si on peut se deplacer l� (=) il n'y a rien dans cette case 
	 * 			FALSE = si on ne peut pas se deplacer l� ou que c'est en dehors de la carte
	 */
	public static boolean peutSeDeplLa(int px , int py)
	{
		boolean onPeut = false ;
		
		if ((0 <= px && px < Carte.getLarg()) && (0 <= py && py < Carte.getHaut()))
			if ( Carte.getAgentPos(px, py) == null )
				onPeut = true ;
		
		return onPeut ;
	}


	
	
	// ============================================================================================================================ //
	

		
	
	/**
	 * Largeur de la carte
	 */
	private static int larg ;
	
	/**
	 * Hauteur de la carte
	 */
	private static int haut ;
	
	/**
	 * Nombre de dragons sur la carte � l'initialisation
	 */
	private static int nbDragonInit;
	
	/**
	 * Nombre de villages sur la carte � l'initialisation
	 */
	private static int nbVillageInit;

	
	/**
	 * M�thode permettant d'initialiser la carte vide
	 */
	public static void newCarte()
	{	
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		
		// Init de la carte
		Carte.carte = new Agent[Carte.getLarg()][Carte.getHaut()] ;
		for (int i=0 ; i<Carte.getLarg() ; i++) 
			for (int j=0 ; j<Carte.getHaut() ; j++)
				Carte.getCarte()[i][j] = null ;
		
		// init des listes
		Carte.agents   = new ArrayList<Agent>() ;
		Carte.newOrder = new ArrayList<Agent>() ; 
		Carte.aRetirer = new ArrayList<Agent>() ;
		Carte.aAjouter = new ArrayList<Agent>() ;
		Carte.evoPop   = new ArrayList<String[]>() ;
	}

	
	
	/**
	 * M�thode initialise les agents dans la carte et qui lance la routine
	 */
	public static void startSimu()
	{
		// Initialisation de la carte et de ses agents
		Dragon d ;
		Village v ;
		
		for (int i=0 ; i<Carte.nbDragonInit ; i++)
		{
			d = new Dragon() ;
			Carte.genererPositionRandom(d);
			Carte.agents.add(d) ;
		}
		
		for (int i=0 ; i<Carte.nbVillageInit ; i++)
		{
			v = new Village() ;
			Carte.genererPositionRandom(v);
			v.setPop(RandomGenerator.getRand(0 , Village.nbMaxHabitant)) ;
			Carte.agents.add(v) ;
		}
	
		if ( estEnModeNoStop )
		{
			// D�but de la routine doit g�rer l'exception des images qui ne sont pas trouv�e pour la fenetre
			try 
			{ Carte.routine() ; } 
			catch (IOException e) 
			{ e.printStackTrace(); }
			
			System.out.println("fin de la routine") ;
		}
	}
	
	
	
	/**
	 * Routine faisant �voluer les agents et se rappelant elle m�me (INFINI)
	 * @throws IOException 
	 */
	private static void routine() throws IOException
	{
		while (Carte.routineEnCours)
		{
			Agent a ;
			Iterator<Agent> iter = Carte.agents.iterator();
			while (iter.hasNext())
			{	
				a = iter.next() ;
				//System.out.print(a.toString()) ;
				a.actionRoutine() ;
				Carte.newOrder.add( RandomGenerator.getRand(0 , Carte.newOrder.size()) , a ) ;
			}
			
			//System.out.print("\n") ;
			Carte.agents = new ArrayList<Agent>(Carte.newOrder) ;
			Carte.newOrder.clear() ;
			Carte.ajouterToutAgentNee() ;
			Carte.retirerToutAgentsMort() ;
			savePopAllAgents() ;
			

			//update panel qui est appel� pour afficher la fenetre a chaque fois et qui throw ioexception si image pas trouv�e
			updatePanel();
			//System.out.println(affConsole()) ;
			
			try 
			{ Thread.sleep(500); } 
			catch (InterruptedException e) 
			{ e.printStackTrace(); }
		}
		
		Carte.saveAllData();
	}
	
	
	
	public static void routinePasParPas()
	{
		Agent a ;
		Iterator<Agent> iter = Carte.agents.iterator();
		while (iter.hasNext())
		{	
			a = iter.next() ;
			//System.out.print(a.toString()) ;
			a.actionRoutine() ;
			Carte.newOrder.add( RandomGenerator.getRand(0 , Carte.newOrder.size()) , a ) ;
		}
		
		//System.out.print("\n") ;
		Carte.agents = new ArrayList<Agent>(Carte.newOrder) ;
		Carte.newOrder.clear() ;
		Carte.ajouterToutAgentNee() ;
		Carte.retirerToutAgentsMort() ;
		savePopAllAgents() ;
	}
	
	
	
	/**
	 * Cette fonction permet de poser un agent sur la carte de mani�re al�atoire (et de mettre � jour convenablement la carte en fonction) 
	 * 
	 * @param a	Agent qu'on va positionner � un endroit al�atoire pour commencer la simulation
	 */
	private static void genererPositionRandom(Agent a)
	{
		int px = RandomGenerator.getRand(0 , Carte.getLarg()-1) ;
		int py = RandomGenerator.getRand(0 , Carte.getHaut()-1) ;
		
		Carte.getCarte()[px][py] = a ;
		a.setPos(px, py) ;
	}
	
	
	
	/**
	 * M�thode permettant d'afficher la carte sous forme d'un string
	 * 
	 * @return Un texte repr�sentant la carte actuellement
	 */
	private static String affConsole()
	{
		StringBuilder builder = new StringBuilder( "" ); 
		StringBuilder line = new StringBuilder( "" ); 
		
		for (int i=0 ; i<Carte.getLarg() ; i++) 
			line.append("----") ;
		line.append("-") ;
		
		builder.append(line.toString()).append("\n") ;
		for (int i=0 ; i<Carte.getLarg() ; i++) 
		{
			for (int j=0 ; j<Carte.getHaut() ; j++)
			{
				if (Carte.getCarte()[i][j] == null)
				{
					if (j == 9) 
						builder.append("|   |");
					else 
						builder.append("|   ") ;
				}
				else
					builder.append("|").append(Carte.getCarte()[i][j].toString()) ;
			}
			builder.append("\n").append(line.toString()).append("\n") ;
		}
		
		return builder.toString() ;
	}
	
	
	
	/**
	 * M�thode permettant d'afficher l'�tat de tout les agents sous forme d'un String
	 * 
	 * @return Un texte repr�sentant tout les agents
	 */
	private static String affConsoleListe()
	{
		StringBuilder line = new StringBuilder( "" );
		Agent a ;
		
		Iterator<Agent> iter = Carte.agents.iterator();
		while (iter.hasNext())
		{
			a = iter.next() ;
			
			if ( a instanceof Dragon )
				line.append( "d=" ) ;
			else
				line.append( "v=" ) ;
			
			line.append( a.getX() + " " + a.getY() ) ;
			
			line.append( " --- " ) ;
		}
		
		return line.toString() ;
	}

	
	
	
	// ================================================= //


	public static String pathSaveData ;
	
	private static ArrayList<String[]> evoPop ;
	
	
	private static void saveAllData()
	{
		Agent a ;
		Iterator<Agent> iter = Carte.agents.iterator();
		while (iter.hasNext())
		{
			a = iter.next() ;
			a.saveVarRecolte() ;
		}
		
		String[] temp ;
		File file = new File(Carte.pathSaveData + "\\EvoPop.csv");
		  
	    try 
	    {
	        // create FileWriter object with file as parameter
	        FileWriter outputfile = new FileWriter(file);
	  
	        // create CSVWriter with '|' as separator
	        CSVWriter writer = new CSVWriter(outputfile, ',',
	                    CSVWriter.NO_QUOTE_CHARACTER,
	                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
	                    CSVWriter.DEFAULT_LINE_END);
	        
	        
	        List<String[]> data = new ArrayList<String[]>();
	        
	        temp = new String[3] ;
	        temp[0] = "dragon" ;
	        temp[1] = "village" ;
	        temp[2] = "ville" ;
	        data.add(temp) ;
        	for (String[] tab : evoPop)
        		data.add(tab) ;
	        
	        
	        writer.writeAll(data);
	        writer.close();
	    }
	    catch (IOException e) 
	    { e.printStackTrace(); }
	}
	
	
	
	private static void savePopAllAgents()
	{
		int nbDragon = 0 , nbVillage = 0 , nbVille = 0 ;
		
		Agent a ;
		Iterator<Agent> iter = Carte.agents.iterator();
		while (iter.hasNext())
		{
			a = iter.next() ;
			
			if ( a instanceof Dragon )
				nbDragon++ ;
			else if ( a instanceof Ville )
				nbVille++ ;
			else if ( a instanceof Village )
				nbVillage++ ;
		}
		
		evoPop.add( new String[] { nbDragon+"" , nbVillage+"" , nbVille+"" } ) ;
	}
	
	
	
	// ================================================= //
	
	
	
	
	public static ArrayList<Agent> getAgents()
	{ return agents ; }
	
	public static void setAgents(ArrayList<Agent> pa)
	{ agents = pa ; }
	
	
	
	public static int getLarg() 
	{ return larg; }
	
	public static void setLarg(int pLarg)
	{ larg = pLarg ; }
	
	

	public static Agent[][] getCarte() 
	{ return carte; }
	
	public static void setCarte(Agent[][] pa) 
	{ carte = pa; }
	
	

	public static int getHaut() 
	{ return haut; }
	
	public static void setHaut(int pHaut) 
	{ haut = pHaut ; }
	
	
	
	public static int getnbDragonInit() 
	{ return nbDragonInit; }
	
	public static void setnbDragonInit(int pnbDragonInit) 
	{ nbDragonInit = pnbDragonInit ; }
	
	
	
	public static int getnbVillageInit() 
	{ return nbVillageInit; }
	
	public static void setnbVillageInit(int pnbVillageInit) 
	{ nbVillageInit = pnbVillageInit ; }

}
