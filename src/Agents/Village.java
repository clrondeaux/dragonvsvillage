package Agents;

import dragonVSvillage.Carte;
import dragonVSvillage.RandomGenerator;

public class Village extends Agent 
{
	
	int population;
	public static final int nbMaxHabitant = 100;
	public static final int evolHabParCycle = 5;
	public static final int probaMigration = 20;
	public static final double nbPerteMaxDansCombat = 20 ;
	
	public int getPop()
	{ return this.population ; }
	
	public void setPop(int n)
	{ this.population = n;}
	
	public Village()
	{ super() ; }
	
	protected void action()
	{
		this.croissancePop();
	}
	
	private void croissancePop()
	{
		//un village peut contenir au max nbMaxHabitant habitants du coup
		//si on a moins de nbMaxHabitant
		if (this.getPop() < nbMaxHabitant)
		{
			//on ajoute un certain nombre d'habitant par cycle
			this.setPop(this.getPop() + evolHabParCycle);
		}
		else 
		{
			this.migrationVillage();
		}
	}
	
	//fonction qui fait que d�s qu'un village est surpeupl� il a une probabilit� 
	//de cr�er un nouveau village dans un rayon de 3 cases autour
	private void migrationVillage()
	{
		int jetMigration = RandomGenerator.getRand(1 , 100);
		if (jetMigration <= probaMigration)
		{
			Village migration = new Village();
			//on g�n�re une position al�atoire entre -3 et 3 autour du village
			int px = -1 , py = -1 ;
			int compt = 0;
			while (!Carte.peutSeDeplLa(px, py) && compt < 3) 
			// on g�n�re une position al�atoire tant qu'on ne peut pas y �tre et tant qu'on n'a pas essay�
			{
				while (px == 0 && py == 0)
				{
					px = RandomGenerator.getRand(-3 , 3) ;
					py = RandomGenerator.getRand(-3 , 3) ;
				}
				
				px = this.getX()+px ;
				py = this.getY()+py ;
				
				++compt;
			}
			//si la case vis�e est vide on g�n�re un nouveau village sur cette case
			if (compt < 3)
			{
				//le village de base divise sa population par deux lors de la migration, mais pour le moment �a acc�l�re le bazar
				migration.setPop((int)Math.floor(this.population/2));
				this.setPop((int)Math.floor(this.population/2));
				migration.setPos(px, py);
				Carte.ajouterAgent(migration) ;
				Carte.naissanceAgent(migration);
			}
		}
	}
	
	
	
	protected boolean seDefend(double degDifficulte)
	{
		boolean succe = false ;
		
		if (this.population <= nbMaxHabitant/3)
		{
			succe = RandomGenerator.getRand() >= degDifficulte ;
		}
		else if (this.population <= nbMaxHabitant/3*2)
		{
			succe = RandomGenerator.getRand() >= degDifficulte ;
			if ( !succe )
				succe = RandomGenerator.getRand() >= degDifficulte ;
		}
		else 
		{
			succe = RandomGenerator.getRand() >= degDifficulte ;
			if ( !succe )
				succe = RandomGenerator.getRand() >= degDifficulte ;
			else if ( ! succe )
				succe = RandomGenerator.getRand() >= degDifficulte ;
		}
		
		// S'il n'y a aucun succ�s, le village est en danger de mort
		if ( !succe )
			this.risqueMourir() ;
		// Sinon ils perdent quand me�m quelques soldats dans la bataille
		else
		{
			this.population -= (int) nbPerteMaxDansCombat*RandomGenerator.getRand(0 , degDifficulte*1.5) ;
		}
		
		return succe ;
	}
	
	
	
	private void risqueMourir()
	{
		Carte.mortAgent(this);
	}
	
	
	
	public String toString()
	{
		return " v " ;
	}
	
	
	
	
	
	
	// ================================================= //
	// 		Zone permettant la r�colte de donn�e
	// ================================================= //
	public void saveVarRecolte()
	{
		
	}
}
