package Agents;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;

import InterfaceGraphique.DragonImage;
import dragonVSvillage.Carte;
import dragonVSvillage.RandomGenerator;



public class Dragon extends Agent 
{
	/**
	 * Reprsente la force d'attaque du dragon
	 */
	private double puissanceAttaque ;
	
	public double getPuissanceAttaque()
	{ return this.puissanceAttaque ; }
	
	public void setPuissanceAttaque( double puiss )
	{ this.puissanceAttaque = puiss ; }
	
	
	
	/**
	 * Reprsente le temps de recharge qu'un dragon a avant de pouvoir refaire des bb
	 * ( Peut etre du au fait qu'il doit attendre aprs eu un bb ou qu'il vient de natre et quen tant que bb il doit attendre de devenir majeur )
	 */
	private int tempsRecupApresBebe ;
	
	public int getTempsRecupApresBebe()
	{ return this.tempsRecupApresBebe ; }
	
	public void setTempsRecupApresBebe(int tps)
	{ this.tempsRecupApresBebe = tps ; }
	
	
	
	/*
	 * Apr�s avoir perdu un combat, le dragon �vite de reprendre un combat pendant X tours
	 */
	private int tempsFuite ;
	
	public int getTempsFuite()
	{ return this.tempsFuite ; }
	
	public void setTempsFuite(int tps)
	{ this.tempsFuite = tps ; }
	
	
	
	/**
	 * On associe al�atoirement � chaque dragon une image de dragon pour le reppr�senter, cet id permet d'identifier quel image il a 
	 */
	private int idImageDuDragon ;
	
	public int getIdImageDuDragon()
	{ return this.idImageDuDragon ; }
	
	
	private static final double AUGM_FACT_PUISS = 1.10 ;
	private static final double PUISS_ATTAQUE_MAX = 5.0 ;
	private static final double SEUIL_MORT = 0.4 ;
	
	private static final int TEMPS_MAX_RECUP_APRES_BEBE = 4 ;
	private static final int TEMPS_BEBE_A_GRANDIR = 6 ;
	
	private static final int TEMPS_MAX_FUITE = 5 ;
	private static final int TEMPS_MIN_FUITE = 2 ;
	
	public Dragon()
	{ 
		super() ;
		this.puissanceAttaque = 1.0 ;
		this.tempsRecupApresBebe = -1 ;
		this.tempsFuite = -1 ;
		this.idImageDuDragon = RandomGenerator.getRand(0 , DragonImage.getNbMaxImgDragonAvailable()) ;
		
		this.initVarRecolte() ;
	}
	
	
	

	
	
	protected void action()
	{
		this.tempsRecupApresBebe-- ;
		this.tempsFuite-- ;
		// Au debut de la journe, la puissance du dragon augmente
		this.puissanceAttaque *= Dragon.AUGM_FACT_PUISS ;
		if ( this.puissanceAttaque > Dragon.PUISS_ATTAQUE_MAX )
			this.puissanceAttaque = Dragon.PUISS_ATTAQUE_MAX ;
		
		Village villageACote = this.aVillageACote() ;
		Dragon dragonACote = this.aDragonACote();
		
		if ( this.tempsFuite < 0 && villageACote != null )
		{
			//System.out.println("On attaque (" + this.puissanceAttaque + ")") ;
			this.attaquerVillage(villageACote) ;
		}
		// Sinon s'il y a un dragon a ct, un bb ne
		else if ( dragonACote != null && (dragonACote.tempsRecupApresBebe<=0 && this.tempsRecupApresBebe<=0) )
		{
			//System.out.println(dragonACote.getIdImageDuDragon() + " et " + this.idImageDuDragon + "font un bebe") ;
			this.spawnOffspringDragons(dragonACote);
			this.seDepl() ;
		}
		// Sinon, le dragon se deplace
		else
			this.seDepl() ;
		
		
		this.savePos() ;
	}
	
	
	
	/*
	 * fonction qui permet de gnrer un bb dragon qui a une force prise alatoirement dans l'intervalle de ses parents
	 */
	private void spawnOffspringDragons(Dragon compagnon)
	{
		//on gnre une position aleatoire initiale  cote du dragon considr comme courant
		int px = -1 , py = -1 ;
		//si la case vise est vide on gnre un bb dragon, sinon on recommence l'alatoire
		int compt = 0;
		while (!Carte.peutSeDeplLa(px, py) && compt < 3) 
		// on gnre une position alatoire tant qu'on ne peut pas y tre et tant qu'on n'a pas essay
		//3 fois
		{
			while (px == 0 && py == 0)
			{
				px = RandomGenerator.getRand(-1 , 1) ;
				py = RandomGenerator.getRand(-1 , 1) ;
			}
			
			px = this.getX()+px ;
			py = this.getY()+py ;
			
			++compt;
		}
		
		//si on a trouv un emplacement libre
		if (compt < 3)
		{
			//on genere le bebe et ses stats
			Dragon enfant = new Dragon();
			enfant.puissanceAttaque = RandomGenerator.getRand(compagnon.puissanceAttaque , this.puissanceAttaque);
			enfant.tempsRecupApresBebe = TEMPS_BEBE_A_GRANDIR ;
			enfant.setPos(px, py); 
			Carte.ajouterAgent(enfant);
			Carte.naissanceAgent(enfant);
			
			// On met un temps de recharge aux parents
			this.tempsRecupApresBebe = RandomGenerator.getRand(1 , TEMPS_MAX_RECUP_APRES_BEBE) ;
			compagnon.tempsRecupApresBebe = RandomGenerator.getRand(1 , TEMPS_MAX_RECUP_APRES_BEBE) ;
		}	
	}
	
	
	private void attaquerVillage(Village v)
	{
		// En fonction taille pop, entre 1 et 3 jet de defense
		// Et la force du dragon dfinit le degrs de difficult du jet de dfense
		// protected boolean seDefend(float degDifficulte) 
		
		//System.out.println("dragon:" + this.puissanceAttaque + " VS village:" + v.getPop()) ;
		
		if ( v.seDefend( this.calcDegDifficulte() ) )
		{
			this.puissanceAttaque = this.puissanceAttaque / 2 ;
			this.tempsFuite = RandomGenerator.getRand(TEMPS_MIN_FUITE, TEMPS_MAX_FUITE) ;
			//System.out.println("Le dragon a perdu, il fuit pendant "+this.tempsFuite);
			// Le dragon est trop faible aprs ce combat, et meurt
			if ( this.puissanceAttaque < Dragon.SEUIL_MORT )
				Carte.mortAgent(this);
		}
		else
		{
			//System.out.println("Le dragon a gagne");
			this.puissanceAttaque = this.puissanceAttaque * RandomGenerator.getRand(0.7 , 0.9) ;
		}
	}
	
	
	 
	private double calcDegDifficulte()
	{ return this.puissanceAttaque / Dragon.PUISS_ATTAQUE_MAX ; }
	
	
	
	/**
	 * Methode permettant de savoir (et de rcuprer) s'il y a un village juste a cote du dragon 
	 * 
	 * @return MAY RETURN NULL - retourne un potentiel village voisin du dragon
	 */
	private Village aVillageACote()
	{
		Agent a ;
		a = Carte.getAgentPos(this.x+1 , this.y) ;
		if (a instanceof Village) // Si a est null, test est faux aussi
			return (Village) a ;
		
		a = Carte.getAgentPos(this.x-1 , this.y) ;
		if (a instanceof Village) 
			return (Village) a ;
		
		a = Carte.getAgentPos(this.x , this.y+1) ;
		if (a instanceof Village) 
			return (Village) a ;
		
		a = Carte.getAgentPos(this.x , this.y-1) ;
		if (a instanceof Village) 
			return (Village) a ;
		
		return null ;
	}
	
	
	

	/**
	 * Methode permettant de savoir (et de rcuprer) s'il y a un dragon juste a cote du dragon 
	 * 
	 * @return MAY RETURN NULL - retourne un potentiel dragon voisin du dragon
	 */
	private Dragon aDragonACote()
	{
		Agent a ;
		a = Carte.getAgentPos(this.x+1 , this.y) ;
		if (a instanceof Dragon) // Si a est null, test est faux aussi
			return (Dragon) a ;
		
		a = Carte.getAgentPos(this.x-1 , this.y) ;
		if (a instanceof Dragon) 
			return (Dragon) a ;
		
		a = Carte.getAgentPos(this.x , this.y+1) ;
		if (a instanceof Dragon) 
			return (Dragon) a ;
		
		a = Carte.getAgentPos(this.x , this.y-1) ;
		if (a instanceof Dragon) 
			return (Dragon) a ;
		
		return null ;
	}
	
	
	
	
	/**
	 * Methode permettant de se deplacer alatoirement d'une case  ct
	 */
	private void seDepl() 
	{
		int px = 0 , py = 0 ;
		
		// On genere une nouvelle position voisine alatoire
		while (px == 0 && py == 0)
		{
			px = RandomGenerator.getRand(-1 , 1) ;
			py = RandomGenerator.getRand(-1 , 1) ;
		}
		px = this.getX()+px ;
		py = this.getY()+py ;
		//System.out.println(this.getX() + " " + this.getY() + "   " + px + " " + py) ;
		
		//si la case vise est vide on dplace le dragon dessus
		if (Carte.peutSeDeplLa(px, py))
		{
			//on change la position du dragon
			Carte.enleverAgent(this);
			this.setPos(px, py);
			Carte.ajouterAgent(this);
		}
		//sinon on informe que la case est occupee et on s'occupera du comportement plus tard
		//else 
			//System.out.println("Cette case est occupee") ;
	}
	
	
	
	
	
	
	// ================================================= //
	// 		Zone permettant la r�colte de donn�e
	// ================================================= //
	private int[][] heatMapDeapl ;
	
	private void initVarRecolte()
	{
		this.heatMapDeapl = new int[Carte.getLarg()][Carte.getHaut()] ;
		for (int i=0 ; i<Carte.getLarg() ; i++) 
			for (int j=0 ; j<Carte.getHaut() ; j++)
				this.heatMapDeapl[i][j] = 0 ;
	}
	
	
	
	private void savePos()
	{ this.heatMapDeapl[this.x][this.y]++ ; }
	
	
	
	public void saveVarRecolte()
	{
		String[] temp ;
		File file = new File(Carte.pathSaveData + "\\DragonPath" + this.id + ".csv");
		  
	    try 
	    {
	        // create FileWriter object with file as parameter
	        FileWriter outputfile = new FileWriter(file);
	  
	        // create CSVWriter with '|' as separator
	        CSVWriter writer = new CSVWriter(outputfile, ',',
	                    CSVWriter.NO_QUOTE_CHARACTER,
	                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
	                    CSVWriter.DEFAULT_LINE_END);
	        
	        
	        List<String[]> data = new ArrayList<String[]>();
	        
	        temp = new String[1] ;
	        temp[0] = "cases" ;
	        data.add(temp) ;
        	for (int i=0 ; i<Carte.getHaut() ; i++)
        	{
        		for (int j=0 ; j<Carte.getLarg() ; j++)
        		{
        			temp = new String[1] ;
        			temp[0] =  this.heatMapDeapl[i][j] + "" ;
        			data.add(temp) ;
        		}
        	}
	        
	        
	
	        writer.writeAll(data);
	        writer.close();
	    }
	    catch (IOException e) 
	    { e.printStackTrace(); }
		
	    
	    try 
	    {
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(Carte.pathSaveData + "\\Dragon" + this.id + ".csv"));
	        writer.write("Dragon created on: " + this.getDebX() + " " + this.getDebY() + "\n");
	        writer.write("The dragon lived for " + this.dureeVie + " turn" + "\n");
	        
	        writer.close();
		}
	    catch (IOException e) 
	    { e.printStackTrace(); }
	}
	
	
	
	
	
	// ================================================= //
	// 		 Zone permettant l'affichage console
	// ================================================= //
	
	/**
	 * Affichage du dragon
	 */
	public String toString()
	{
		//return toStringMore() ;
		return " " + this.idImageDuDragon + " " ;
	}
	
	
	public String toStringMore()
	{
		if (this.tempsFuite >= 0)
		{
			if (tempsRecupApresBebe >= 0)
				return this.idImageDuDragon + "- (fuit) (x=" + this.x + " y=" + this.y + ") puissance= " + this.puissanceAttaque + "(pas accouplement avant " + this.tempsRecupApresBebe + ")" ;
			else
				return this.idImageDuDragon + "- (fuit) (x=" + this.x + " y=" + this.y + ") puissance= " + this.puissanceAttaque + "(peut s'accoupler)" ;
		}
		else
		{
			if (tempsRecupApresBebe >= 0)
				return this.idImageDuDragon + "- (fuit pas) (x=" + this.x + " y=" + this.y + ") puissance= " + this.puissanceAttaque + "(pas accouplement avant " + this.tempsRecupApresBebe + ")" ;
			else
				return this.idImageDuDragon + "- (fuit pas) (x=" + this.x + " y=" + this.y + ") puissance= " + this.puissanceAttaque + "(peut s'accoupler)" ;
		}
	}
}
