package Agents;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public abstract class Agent extends JPanel
{
	int x ;
	
	public int getX()
	{ return this.x ; }
	
	
	
	int y ;
	
	public int getY()
	{ return this.y ; }
	
	
	
	
	private int debX , debY ;
	
	protected int getDebX()
	{ return debX ; }
	
	protected int getDebY()
	{ return debY ; }
	
	
	
	
	
	public void setPos(int px , int py)
	{
		this.x = px ;
		this.y = py ;
		
		if (this.debX == -1)
		{
			this.debX = px ;
			this.debY = py ;
		}
	}
	
	protected int dureeVie ;

	protected int id ;
	
	
	// ------------------------------------------------------------------------------------------------------- //
	
	
	
	public Agent()
	{
		this.x = 0 ;
		this.y = 0 ;
		
		this.debX = -1 ;
		this.debY = -1 ;
		
		this.dureeVie = 0 ;
		
		this.id = Agent.idMax ;
		Agent.idMax++ ;
	}
	
	
	
	final public void actionRoutine()
	{
		this.action();
		this.dureeVie++ ;
	}
	
	
	
	/**
	 * C'est appel� dans la routine r�guli�rement
	 * C'est ce que fait l'agent tout les "jours"
	 */
	abstract protected void action() ;
	
	
	
	abstract public String toString() ;
	
	
	final public boolean equals(Object obj)
	{
		boolean estEgal = false ;
		
		if ( obj != null )
		{
			if (obj instanceof Agent)
			{
				if ( ((Agent)obj).getX() == this.getX() && ((Agent)obj).getY() == this.getY() )
					estEgal = true ;
			}
		}
		
		return estEgal ;
	}
	
	
	
	// =======================================================================================
	
	
	
	public abstract void saveVarRecolte() ;
	
	
	private static int idMax = 0 ;
}
