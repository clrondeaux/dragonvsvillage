package InterfaceGraphique;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Agents.Agent;
import Agents.Dragon;
import Agents.Village;
import Agents.Ville;
import dragonVSvillage.Carte;

public class MyWindow extends JFrame implements ActionListener{
	/**
	 * correction faite par eclipse
	 */
	private static final long serialVersionUID = -5529336786288984928L;
	
	
	private static JLabel[][] labelGrid;
	
	//zones de saisie pour les nombres.
	private static JTextField nbVillages = new JTextField(16);
	private static JTextField nbDragons = new JTextField(16);
	
	//string pour utiliser le cardlayout
	final static String ENTRYPANEL = "1";
	final static String CARTEPANEL = "2";
 
	//le card layout permet de flip entre les deux panels
	private static CardLayout gestionnaireDesPanels = new CardLayout();
	private static JPanel parentPanel = new JPanel();
	protected static JPanel courPanel;
	
	

	public MyWindow()
	{
		super("DragonVSVillage");
		//permet de faire quelque chose de plus propre que de juste
		//quitter l'application quand on ferme la fenetre
		//rend toutes les ressources de la fenetre fermee
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//choix de la taille de base de la fenetre
		this.setSize(900,900);
		//choix de la position relative au bureau
		//ne pas changer l'ordre de ces deux derni�res infos 
		this.setLocationRelativeTo(null);
		this.setResizable(false);

		
		
		addWindowListener(new java.awt.event.WindowAdapter() 
		{
            public void windowClosing(java.awt.event.WindowEvent e) 
            {
            	Carte.routineEnCours = false ;
            	setVisible(false);
            }
        });
	}
	
	//fonction qui intialise tous les panels avec les bonnes informations dedans
	public static void initAffichage(MyWindow mw)
	{
		parentPanel.setLayout(gestionnaireDesPanels);
		parentPanel.setPreferredSize(new Dimension(400, 200));
		//cr�ation de la nouvelle carte (elle doit �tre cr�e pour set le panel)
		Carte.newCarte();
		//cr�ation des deux panels n�cessaires (un pour la saisie, un pour afficher la carte)
		JPanel newP1 = formInput(mw);
		JPanel newP2 = setPanel(mw);
		//on ajoute ces panels au panel parent
		parentPanel.add(newP1, ENTRYPANEL);
		parentPanel.add(newP2, CARTEPANEL);
		//on choisit quel panel on affiche en premier
		gestionnaireDesPanels.show(parentPanel, "1");
		courPanel = newP2;
		//on ajoute le panel parent � la fenetre
		mw.add(parentPanel);
	}
	
	//fonction qui renvoie le panel pour la saisie de donn�es
	public static JPanel formInput(MyWindow mw)
	{
		JPanel entryPanel = new JPanel();
		
		if ( mw != null )
		{
			
			JLabel l1 = new JLabel("nombre de villages : ");
			JLabel l2 = new JLabel("nombre de dragons : ");
			JButton b = new JButton("submit");
			
			b.addActionListener(mw);
			
			entryPanel.add(l1);
			entryPanel.add(nbVillages);
			entryPanel.add(l2);
			entryPanel.add(nbDragons);
			entryPanel.add(b);
		}
		
		return entryPanel;
	}
	
	
	//fonction qui renvoie le panel vide pour la carte (cr�� avec la taille de la carte)
	public static JPanel setPanel(MyWindow mw)
	{
		JPanel cartePanel = new JPanel();
		
		if ( mw != null )
		{
			cartePanel.setLayout(new GridLayout(Carte.getLarg(), Carte.getHaut(), -1, -1));
			labelGrid = new JLabel[Carte.getLarg()][Carte.getHaut()];
			
			//on initialise avec des bodures et des labels vides (contiendront les images)
			for (int i=0 ; i<Carte.getLarg() ; i++) 
			{
				for (int j=0 ; j<Carte.getHaut() ; j++)
				{
					labelGrid[i][j] = new JLabel("");
					labelGrid[i][j].setBorder(BorderFactory.createLineBorder(Color.BLACK));
					cartePanel.add(labelGrid[i][j]);
				}
			}
		}
		
		return cartePanel;
	}
	
	
	
	//a chaque fois on update en fonction des positions des agents
	//throw ioexception quand les images qu'on essaye de charger ne sont pas trouv�es (g�r�e dans carte)
	public static void updatePanel() throws IOException
	{	
		Image image;
		ImageIcon icon = null;
		Agent a ;
		for (int i=0 ; i<Carte.getLarg() ; i++) 
		{
			for (int j=0 ; j<Carte.getHaut() ; j++)
			{
				a = Carte.getAgentPos(i , j) ;
				if (a != null)
				{
					
					if (a instanceof Village)
					{
						if (a instanceof Ville)
							image = ImageIO.read(new File("images\\city.png"));
						else
							image = ImageIO.read(new File("images\\house.png"));
							//on ajoute l'image de village a l'emplacement correspondant entre la carte et la grille
						
					}
					else //(a instanceof Dragon)
						//sinon l'image du dragon
						image = DragonImage.getImageOfThisDragon( (Dragon) a ) ;
					
				}
				else 
					image = ImageIO.read(new File("images\\blank.png"));
					//sinon une image vide
				
				
				image = image.getScaledInstance(90 , 90 , Image.SCALE_SMOOTH);
				icon = new ImageIcon(image);
				
				if (icon != null)
				{
					//on donne l'image au label en charge de l'afficher
					labelGrid[i][j].setIcon(icon);
				}
			}
		}
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		String s = e.getActionCommand();
        if (s.equals("submit")) {
        	String nbDragonInit = nbDragons.getText();
        	String nbVillageInit = nbVillages.getText();
        	if (nbDragonInit != "")
        	{
        		if (nbVillageInit != "")
        		{
        			//on recupere les infos du nombre de dragons et de villages de d�part
        			Carte.setnbDragonInit(Integer.parseInt(nbDragons.getText()));
                	Carte.setnbVillageInit(Integer.parseInt(nbVillages.getText()));
                	//on change quel panel on veut afficher
                	gestionnaireDesPanels.show(parentPanel, "2");
                	//necessit� de cr�er un thread pour que l'interface puisse tjrs afficher, mais t�che longue de simu en fond
                	Thread t = new Thread() {
        	            public void run() {
        	              Carte.startSimu(); 
        	            }
                     };
                     t.start();
                }
	    		else 
	    		{
	    			System.out.print("Nb de village pas init");
	    		}
        	}
	    	else 
	    	{
	    		System.out.print("Nb de dragon pas init");
	    	}
		}
	}
}
