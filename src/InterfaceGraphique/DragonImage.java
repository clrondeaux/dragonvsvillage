package InterfaceGraphique;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import Agents.Dragon;

public class DragonImage 
{

	private static String pathImgDragon = "images\\dragon" ;
	private static String endPathImgDragon = ".png" ;
	private static int nbMaxImgDragonAvailable = 6 ;
	
	public static int getNbMaxImgDragonAvailable()
	{ return nbMaxImgDragonAvailable ; }
	
	
	public static Image getImageOfThisDragon(Dragon d)
	{
		Image i = null ;
		
		try 
		{
		if (d != null)
				i = ImageIO.read(new File( pathImgDragon + d.getIdImageDuDragon() + endPathImgDragon ));
			
		else
			i = ImageIO.read(new File( pathImgDragon + "0" + endPathImgDragon ));
		} 
		catch (IOException e) 
		{
			System.out.println(pathImgDragon + d.getIdImageDuDragon() + endPathImgDragon + " can't be read") ;
			e.printStackTrace(); 
		}
		
		return i ;
	}
}
