import dragonVSvillage.Carte;

import java.io.IOException;

import InterfaceGraphique.MyWindow;

public class Main {

	public static void main(String[] args) 
	{
		Carte.pathSaveData = "..\\recolteData" ;
		
		MyWindow myWindow = new MyWindow();
		MyWindow.initAffichage(myWindow);
		myWindow.setVisible(true);
	}

}
