package tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import Agents.Agent;
import Agents.Dragon;
import Agents.Village;
import dragonVSvillage.Carte;
import dragonVSvillage.RandomGenerator;

class Test 
{
	/*
	 * Verification de la bonne initialisation de la carte  
	 */
	@org.junit.jupiter.api.Test
	void test_initCarte() 
	{
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		Carte.setnbDragonInit(5);
		Carte.setnbVillageInit(4);
		Carte.estEnModeNoStop = false ;
		Carte.newCarte();
		Carte.startSimu();
		assertTrue( (String) ("On a " + Carte.getAgents().size() + " au lieu de 9") , Carte.getAgents().size() == 9 ) ;
	}
	
	
	
	/**
	 * On verifie que la fonction getRand(int a, int b) retourne bien des valeurs [a , b], a et b compris et jamais en dehors
	 */
	@org.junit.jupiter.api.Test
	void test_alea()
	{
		int mine = 0 , maxe = 50 ;
		
		int nbMax = 0 ;
		int nbMin = 0 ;
		
		int temp ;
		
		for (int i=0 ; i<1000 ; i++)
		{
			temp = RandomGenerator.getRand(mine, maxe) ;
			if (temp > maxe)
				assertTrue("On a eu plus que le max: " + temp , false) ;
			else if (temp < mine)
				assertTrue("On a eu moins que le min: " + temp , false) ;
			
			if (temp == maxe)
				nbMax ++ ;
			
			if (temp == mine)
				nbMin ++ ;
		}
		
		assertTrue("On a pas eu une seule valeur en min" , nbMin!=0) ;
		assertTrue("On a pas eu une seule valeur en max" , nbMax!=0) ;
	}

	
	
	/**
	 * Test v�rifiant que le dragon ne se d�place que d'une case par tours
	 */
	@org.junit.jupiter.api.Test
	void test_deplDragon()
	{
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		Carte.setnbDragonInit(0);
		Carte.setnbVillageInit(0);
		Carte.estEnModeNoStop = false ;
		Carte.newCarte();
		
		ArrayList<Agent> a = new ArrayList<Agent>() ;
		Dragon d = new Dragon() ;
		d.setPos(Carte.getLarg()/2 , Carte.getHaut()/2);
		a.add(d) ;
		
		Carte.ajouterAgent(d);
		Carte.setAgents(a);
		int px , py , nbTimeBougePas = 0 ;
		
		assertTrue("Le dargon c'est deplacer avant le debut de la routine" , d.getX() == Carte.getLarg()/2 && d.getY() == Carte.getHaut()/2 ) ;
		
		for (int i=0 ; i<100 ; i++)
		{
			px = d.getX() ;
			py = d.getY() ;
		
			Carte.routinePasParPas() ;
			
			if ( !(px-1 <= d.getX() && d.getX() <= px+1) )
				assertTrue("x avant: " + px + " et apres: " + d.getX() , false) ;
			
			if ( !(py-1 <= d.getY() && d.getY() <= py+1) )
				assertTrue("y avant: " + py + " et apres: " + d.getY() , false) ;
			
			if ( d.getX() == px && d.getY() == py )
				nbTimeBougePas++ ;
		}
		
		System.out.println("le dragon n'a pas boug� " + nbTimeBougePas + " fois") ;
	}
	
	
	/**
	 * Test v�rifiant que le dragon se reproduit toujours quand un autre dragon est � c�t� 
	 */
	@org.junit.jupiter.api.Test
	void test_reproDragon1()
	{
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		Carte.setnbDragonInit(0);
		Carte.setnbVillageInit(0);
		Carte.estEnModeNoStop = false ;
		Carte.newCarte();
		
		ArrayList<Agent> a = new ArrayList<Agent>() ;
		
		Dragon d1 = new Dragon() ;
		d1.setPos(5 , 5);
		a.add(d1) ;
		Carte.ajouterAgent(d1);
		
		Dragon d2 = new Dragon() ;
		d2.setPos(5 , 6);
		a.add(d2) ;
		Carte.ajouterAgent(d2);
	
		Carte.setAgents(a);
		
		
		
		Carte.routinePasParPas() ;
		
		ArrayList<Agent> aa = Carte.getAgents() ;
		assertTrue("Les dragon auraient du se reproduire et on devrait avoir 3 dragon maintenant, il y en a " + aa.size() , aa.size() == 3) ;
		
		aa = Carte.getAgents() ;
		aa.get(0).setPos(5 , 5);
		((Dragon) aa.get(0)).setTempsRecupApresBebe(3) ;
		Carte.ajouterAgent(aa.get(0));
		aa.get(1).setPos(5 , 6);
		((Dragon) aa.get(1)).setTempsRecupApresBebe(3) ;
		Carte.ajouterAgent(aa.get(1));
		
		
		
		Carte.routinePasParPas() ;
		
		aa = Carte.getAgents() ;
		assertTrue("Les dragon n'auraient pas du pouvoir se reproduire encore, il y en a " + aa.size()  , aa.size() == 3) ;
	}
	
	
	
	/**
	 * Test v�rifiant que les dragons ne peuvent pas se reproduire en diagonal 
	 */
	@org.junit.jupiter.api.Test
	void test_reproDragon2()
	{
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		Carte.setnbDragonInit(0);
		Carte.setnbVillageInit(0);
		Carte.estEnModeNoStop = false ;
		Carte.newCarte();
		
		ArrayList<Agent> a = new ArrayList<Agent>() ;
		
		Dragon d1 = new Dragon() ;
		d1.setPos(5 , 5);
		a.add(d1) ;
		Carte.ajouterAgent(d1);
		
		Dragon d2 = new Dragon() ;
		d2.setPos(6 , 6);
		a.add(d2) ;
		Carte.ajouterAgent(d2);
	
		Carte.setAgents(a);
		
		
		
		Carte.routinePasParPas() ;
		
		ArrayList<Agent> aa = Carte.getAgents() ;
		assertTrue("Les dragon n'auraient pas du pouvoir se reproduire, il y en a mtn  " + aa.size() , aa.size() == 2) ;
	}
	
	
	
	/**
	 * Test v�rifiant qu un dragon ne peut pas se reproduire seul
	 * + g�n�ratio heatmap d�placement d'un dragon seul
	 */
	@org.junit.jupiter.api.Test
	void test_reproDragon3()
	{
		Carte.pathSaveData = "D:\\Utilisateurs\\Yasistrois\\Desktop\\Cours\\ALQ\\recolteData" ;
		
		for (int ii= 0 ; ii<10 ; ii++)
		{
			Carte.setLarg(10) ;
			Carte.setHaut(10) ;
			Carte.setnbDragonInit(0);
			Carte.setnbVillageInit(0);
			Carte.estEnModeNoStop = false ;
			Carte.newCarte();
			
			ArrayList<Agent> a = new ArrayList<Agent>() ;
			
			Dragon d1 = new Dragon() ;
			d1.setPos(5 , 5);
			a.add(d1) ;
			Carte.ajouterAgent(d1);
			
			Carte.setAgents(a);
			
			for (int i=0 ; i<10000 ; i++)
				Carte.routinePasParPas();
			
			a = Carte.getAgents() ;
			//(a.get(0)).saveVarRecolte() ;
			assertTrue("Le dragon n'aurait pas du pouvoir se reproduire seul, il y en a mtn  " + a.size() , a.size() == 1) ;
		}
	}
	
	
	
	/**
	 * On v�rfiei que quand le dragon est � c�t� d'un village il peut l'attaquer
	 */
	@org.junit.jupiter.api.Test
	void test_attaqueDragon1()
	{
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		Carte.setnbDragonInit(0);
		Carte.setnbVillageInit(0);
		Carte.estEnModeNoStop = false ;
		Carte.newCarte();
		
		ArrayList<Agent> a = new ArrayList<Agent>() ;
		
		Dragon d1 = new Dragon() ;
		d1.setPos(5 , 5);
		d1.setPuissanceAttaque(5.0);
		a.add(d1) ;
		Carte.ajouterAgent(d1);
		
		Village v = new Village() ;
		v.setPos(5 , 6) ;
		v.setPop(1);
		a.add(v) ;
		Carte.ajouterAgent(v);
		
		Carte.setAgents(a);
		
		
		
		Carte.routinePasParPas();
		
		a = Carte.getAgents() ;
		assertTrue("Le dragon aurait du d�truire le village  " + a.size() , a.size() == 1) ;
		assertTrue("Le seul survivant devrait petre le dragon  " + a.get(0) , a.get(0) instanceof Dragon) ;
		assertTrue("Le dragon ne devrait pas fuir apr�s une victoire!" , ((Dragon) a.get(0)).getTempsFuite() < 0  ) ;
	}
	
	
	
	/**
	 * On v�rfie que le village peut survivre a un combat avec un dragon faible
	 */
	@org.junit.jupiter.api.Test
	void test_attaqueDragon2()
	{
		int nbSurvie = 0 ;
		double forceDragon ;
		
		for (int i=0 ; i<10 ; i++)
		{
			Carte.setLarg(10) ;
			Carte.setHaut(10) ;
			Carte.setnbDragonInit(0);
			Carte.setnbVillageInit(0);
			Carte.estEnModeNoStop = false ;
			Carte.newCarte();
			
			ArrayList<Agent> a = new ArrayList<Agent>() ;
			
			Dragon d1 = new Dragon() ;
			d1.setPos(5 , 5);
			d1.setPuissanceAttaque(3.0);
			forceDragon = d1.getPuissanceAttaque() ;
			a.add(d1) ;
			Carte.ajouterAgent(d1);
			
			Village v = new Village() ;
			v.setPos(5 , 6) ;
			a.add(v) ;
			Carte.ajouterAgent(v);
			
			Carte.setAgents(a);
			
			
			
			Carte.routinePasParPas();
			
			assertTrue("Apres un combat, gagne ou perdu, le dragon devrait avoir perdu au moins un peu de puissance" , forceDragon > d1.getPuissanceAttaque()) ;
			a = Carte.getAgents() ;
			if ( a.size() == 2 )
				nbSurvie++ ;
		}
		
		assertTrue("Le dragon n'aurait pas du pouvoir d�truire le village a chaque fois" , nbSurvie > 0) ;
		assertTrue("Le village n'aurait pas du gagner contre le dragon � chaque fois" , nbSurvie < 10) ;
	}
	
	
	
	/**
	 * On v�rifie que quand le dragonn'est pas cote � cote d'un vilalge, il ne peut pas l'attaquer
	 */
	@org.junit.jupiter.api.Test
	void test_attaqueDragon3()
	{
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		Carte.setnbDragonInit(0);
		Carte.setnbVillageInit(0);
		Carte.estEnModeNoStop = false ;
		Carte.newCarte();
		
		ArrayList<Agent> a = new ArrayList<Agent>() ;
		
		Dragon d1 = new Dragon() ;
		d1.setPos(5 , 5);
		d1.setPuissanceAttaque(5.0);
		a.add(d1) ;
		Carte.ajouterAgent(d1);
		
		Village v = new Village() ;
		v.setPos(6 , 6) ;
		v.setPop(1);
		a.add(v) ;
		Carte.ajouterAgent(v);
		
		Carte.setAgents(a);
		
		
		
		Carte.routinePasParPas();
		
		assertTrue("Le dragon ne devrait pas pouvoir attaquer le village de si loin" , Carte.getAgents().size() == 2) ;
	}
	
	
	
	/**
	 * On v�rifie que la population du village augmente bien � chaque tours
	 */
	@org.junit.jupiter.api.Test
	void test_VillageGrandit()
	{
		Carte.setLarg(10) ;
		Carte.setHaut(10) ;
		Carte.setnbDragonInit(0);
		Carte.setnbVillageInit(0);
		Carte.estEnModeNoStop = false ;
		Carte.newCarte();
		
		ArrayList<Agent> a = new ArrayList<Agent>() ;
		
		Village v = new Village() ;
		v.setPos(6 , 6) ;
		v.setPop(1);
		a.add(v) ;
		Carte.ajouterAgent(v);
		
		Carte.setAgents(a);
		
		
		
		Carte.routinePasParPas();
		assertTrue("La population du village aurait du augmenter de 5, mais il est a: " + v.getPop() , v.getPop() == 1+5) ;
		
		

		Carte.routinePasParPas();
		assertTrue("La population du village aurait du augmenter de 5, mais il est a: " + v.getPop() , v.getPop() == 1+5+5) ;
	}
	
	
	
	
	/**
	 * On v�rifie que quand la population d'un vilalge atteind le max, un autre village est cr�e
	 */
	@org.junit.jupiter.api.Test
	void test_VillageEttend()
	{
		int nbFoisSeReprMax = 0 ;
		for (int i=0 ; i<20 ; i++)
		{
			Carte.setLarg(10) ;
			Carte.setHaut(10) ;
			Carte.setnbDragonInit(0);
			Carte.setnbVillageInit(0);
			Carte.estEnModeNoStop = false ;
			Carte.newCarte();
			
			ArrayList<Agent> a = new ArrayList<Agent>() ;
			
			Village v = new Village() ;
			v.setPos(6 , 6) ;
			v.setPop(1);
			a.add(v) ;
			Carte.ajouterAgent(v);
			
			Carte.setAgents(a);
			
			
			
			Carte.routinePasParPas();
			assertTrue("La population du village aurait du augmenter de 5, mais il est a: " + v.getPop() , v.getPop() == 1+5) ;
			
			
			
			v.setPop(100);
			Carte.routinePasParPas();
			if ( Carte.getAgents().size()==2 )
				nbFoisSeReprMax++ ;
		}
		
		assertTrue("Comme le village avait atteind sa population max, il aurait du en cr�er un autre mais il ne le fait jamais"  , nbFoisSeReprMax > 0) ;
		assertTrue("Comme le village avait atteind sa population max, il aurait du en cr�er un autre mais il le fait � chaque fois"  , nbFoisSeReprMax < 20) ;
		
		
		
		int nbFoisSeReprPlusMax = 0 ;
		for (int i=0 ; i<20 ; i++)
		{
			Carte.setLarg(10) ;
			Carte.setHaut(10) ;
			Carte.setnbDragonInit(0);
			Carte.setnbVillageInit(0);
			Carte.estEnModeNoStop = false ;
			Carte.newCarte();
			
			ArrayList<Agent> a = new ArrayList<Agent>() ;
			
			Village v = new Village() ;
			v.setPos(6 , 6) ;
			v.setPop(1);
			a.add(v) ;
			Carte.ajouterAgent(v);
			
			Carte.setAgents(a);
			
			
			
			Carte.routinePasParPas();
			assertTrue("La population du village aurait du augmenter de 5, mais il est a: " + v.getPop() , v.getPop() == 1+5) ;
			
			
			
			v.setPop(162);
			Carte.routinePasParPas();
			if ( Carte.getAgents().size()==2 )
				nbFoisSeReprPlusMax++ ;
		}
		
		assertTrue("Comme le village �tait � plus de sa population max, il aurait du en cr�er un autre mais il ne le fait jamais"  , nbFoisSeReprPlusMax > 0) ;
		assertTrue("Comme le village �tait � plus de sa population max, il aurait du en cr�er un autre mais il le fait � chaque fois"  , nbFoisSeReprPlusMax < 20) ;
	}
}
