import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt



# Read the Pharma Sector data 
df = pd.read_csv("../recolteData/DragonPath15.csv")
symbol = ((np.asarray(df['cases']).reshape(10,10)))
heat_map = sns.heatmap(symbol , vmin=30, vmax=190)


plt.show()

# 30 à 190