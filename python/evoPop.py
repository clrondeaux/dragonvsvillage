import numpy as np
import math
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt



# Read the Pharma Sector data 
df = pd.read_csv("../../recolteData/EvoPop.csv")
dragon = (np.asarray(df['dragon']))
village = (np.asarray(df['village']))
ville = (np.asarray(df['ville']))

arr=[]
arr = [i for i in range(len(dragon))] 


plt.plot(arr, dragon , '#ef233c' ,  label='nombre de dragons')
plt.plot(arr, village , '#48cae4', label='nombre de villages')
plt.plot(arr, ville , '#0077b6', label='nombre de villes')


plt.xlabel('Tours')
plt.ylabel('Nombre d\'agent dans cette population')
plt.title('Evolution des populations')
plt.legend()


plt.show()

# 30 à 190